# flask --app lab01  --debug run
#from flask import Flask
from flask import Flask, render_template
from markupsafe import escape
app = Flask(__name__)

@app.route('/')
def index():
    return 'Index Page'

@app.route('/hello')
def hello():
    return 'Hello, World'

@app.route("/times/", defaults={ 'm': '2' })
@app.route('/times/<m>')
def times(m):
    result = '<table border=0>'
    for i in range(13):
      if(i>0):
        ans = int(m)*i
        result = result+'<tr><td>'+str(m)+'x'+str(i)+'='+str(ans)+'</td></tr>'

    result = result + '</table>'    
    
 
    #return result
    title = 'times table '+m
    return render_template('ui.html',title=title,m=m,result=result)