# flask --app lab01  --debug run
from flask import Flask

app = Flask(__name__)

@app.route('/')
def index():
    return 'Index Page'

@app.route('/hello')
def hello():
    return 'Hello, World'

@app.route('/times/<m>')
def times(m):
    result = '<table>'
    for i in range(13):
      if(i>0):
        ans = int(m)*i
        result = result+'<tr><td>'+str(m)+'x'+str(i)+'='+str(ans)+'</td></tr>'

    result = result + '</table>'    
    return result