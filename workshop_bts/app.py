from flask import Flask, render_template, request
import pandas as pd 
def route(index_goal_line,index_start_line,start_orderList,goal_orderList,all_line,ans):
      #if(index_goal_line==index_start_line): #กรณีที่เราไม่ต้องเปลี่ยนสถานี
      if(start_orderList <= goal_orderList):
         for i in range(len(all_line[index_goal_line])):
            #print(all_line[index_goal_line]['StationNameTH'][i])
            if( all_line[index_goal_line]['Orderlist'][i]>=start_orderList and 
                all_line[index_goal_line]['Orderlist'][i]<=goal_orderList):
                   #print(all_line[index_goal_line]['StationNameTH'][i])
                   z = all_line[index_goal_line]['StationNameTH'][i] 
                   if(not z in ans): 
                     ans.append(z) 


      if(start_orderList >= goal_orderList):
         for i in range(len(all_line[index_goal_line])):
            #print(all_line[index_goal_line]['StationNameTH'][i])
            r_i = len(all_line[index_goal_line])-i-1
            if( all_line[index_goal_line]['Orderlist'][r_i]<=start_orderList and 
                all_line[index_goal_line]['Orderlist'][r_i]>=goal_orderList):
                   #print(all_line[index_goal_line]['StationNameTH'][r_i])
                   z = all_line[index_goal_line]['StationNameTH'][r_i] 
                   if(not z in ans): 
                     ans.append(z) 
                    
      return ans

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def index():
    # ตัวอย่างข้อมูลตัวเลือก

    Nline = pd.read_csv("Nline.csv")
    Sline = pd.read_csv("Sline.csv")
    Eline = pd.read_csv("Eline.csv")
    Wline = pd.read_csv("Wline.csv")

    a = list(Nline['StationNameTH'].values)
    b = list(Sline['StationNameTH'].values)
    c = list(Eline['StationNameTH'].values)
    d = list(Wline['StationNameTH'].values)

    line = a+b+c+d

    new_line = []
    for i in line:
        if(not i in new_line):
            new_line.append(i)
    choices = new_line
    choices2 = new_line.copy()

    return render_template('index.html', choices=choices, choices2=choices2)

@app.route('/showpath', methods=['POST'])
def upload():
    if request.method == 'POST':
        choices = request.form['choices']
        choices2 = request.form['choices2']

        start = choices
        goal = choices2

        Nline = pd.read_csv("Nline.csv")
        Sline = pd.read_csv("Sline.csv")
        Eline = pd.read_csv("Eline.csv")
        Wline = pd.read_csv("Wline.csv")

        all_line = [Nline,Sline,Eline,Wline]
        name_line = ['Nline','Sline','Eline','Wline']

        start_orderList = -1
        goal_orderList = -1
        index_start_line = -1
        index_goal_line = -1

        # ชั้นจะตรวจสอบว่า ต้นทาง อยู่ที่สายไหน
        for i in range(len(all_line)):
            k = all_line[i]['StationNameTH']
            for j in range(len(k)):
                if(start == k[j]):
                    start_orderList = all_line[i]['Orderlist'][j]
                    index_start_line = i
                    #print(name_line[i],start_orderList)
                

        print('---')
                
        # ชั้นจะตรวจสอบว่า ปลายทาง อยู่ที่สายไหน
        for i in range(len(all_line)):
            k = all_line[i]['StationNameTH']
            for j in range(len(k)):
                if(goal == k[j]):
                    goal_orderList = all_line[i]['Orderlist'][j]
                    index_goal_line = i
                    #print(name_line[i],goal_orderList)

        if(index_goal_line==index_start_line): 
          ans = []  
          ans = route(index_goal_line,index_start_line,start_orderList,goal_orderList,all_line,ans)  
  
        else:
          ans = []  
          ans = route(index_start_line,index_start_line,start_orderList,0,all_line,ans)  #start at start to siam
          if(goal_orderList!=0): #fix bug case goal is siam 
            ans = route(index_goal_line,index_goal_line,1,goal_orderList,all_line,ans)  #start at siam to goal

        
        return render_template('showpath.html',choices=choices, choices2=choices2 ,ans=ans)

if __name__ == '__main__':
    app.run(debug=True)