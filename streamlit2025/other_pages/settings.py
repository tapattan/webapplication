import streamlit as st

def app():
    st.title("หน้าตั้งค่า")
    st.write("นี่คือหน้าสำหรับตั้งค่าของแอปพลิเคชัน")
    # ตัวอย่างแบบฟอร์ม
    username = st.text_input("ชื่อผู้ใช้")
    password = st.text_input("รหัสผ่าน", type="password")
    if st.button("บันทึกการตั้งค่า"):
        st.success("บันทึกการตั้งค่าเรียบร้อย!")
