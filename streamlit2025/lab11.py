import streamlit as st

# Inject CSS เพื่อปรับแต่งสีปุ่ม
 
st.title("🧮 เครื่องคิดเลขใน Streamlit")

def click_button(name):
    if(name=='AC'):
      st.session_state.current_input = '0'

    elif(name=='Del'):
       
        if(len(st.session_state.current_input)==2 and st.session_state.current_input [0]=='-'):
           st.session_state.current_input = '0'

        if(len(st.session_state.current_input)==1):
           st.session_state.current_input = '0'

        st.session_state.current_input = st.session_state.current_input[0:-1]   

        if(st.session_state.current_input ==''):
           st.session_state.current_input = '0' 

    elif(name=='percent'):
       st.session_state.current_input = str(float(st.session_state.current_input)/100)

    elif(name=='add_subtract'):
        k = float(st.session_state.current_input)*-1
        if(k%1==0):
          k = int(k) 

        st.session_state.current_input = str(k)


    elif(name=='='):
      if('x' in st.session_state.current_input):
        st.session_state.current_input = st.session_state.current_input.replace('x','*')
      if('÷' in st.session_state.current_input):
        st.session_state.current_input = st.session_state.current_input.replace('÷','/')  

      ans = eval(st.session_state.current_input)   
      st.session_state.current_input = str(ans)

    else:
       if(st.session_state.current_input[0]=='0'):
         st.session_state.current_input = st.session_state.current_input[1:] 

       st.session_state.current_input += name   


# สร้างพื้นที่แสดงผลด้วย st.empty()
display = st.empty()

# สร้างตัวแปรเก็บสถานะการแสดงผล
if "current_input" not in st.session_state:
    st.session_state.current_input = '0'

# แสดงผลในพื้นที่ว่าง
display.markdown(f"<h1 style='text-align: right;'>{st.session_state.current_input}</h1>", unsafe_allow_html=True)

 
# สร้างปุ่มสำหรับตัวเลขและเครื่องหมายในรูปแบบตาราง
col1, col2, col3, col4 = st.columns(4)

# แถวที่ 1
with col1:
    st.button("AC", key="AC",on_click=click_button, args=['AC'])
with col2:
    st.button("Del", key="Del",on_click=click_button, args=['Del'])
with col3:
    st.button("+/-", key="add_subtract",on_click=click_button, args=['add_subtract'])
with col4:
    st.button("÷", key="divide" ,on_click=click_button, args=['÷'])

# แถวที่ 2
with col1:
    st.button("7", key="7",on_click=click_button, args=['7'])
with col2:
    st.button("8", key="8",on_click=click_button, args=['8'])
with col3:
    st.button("9", key="9",on_click=click_button, args=['9'])
with col4:
    st.button("×", key="multiply",on_click=click_button, args=['x'])

# แถวที่ 3
with col1:
    st.button("4", key="4" ,on_click=click_button, args=['4'])
with col2:
    st.button("5", key="5",on_click=click_button, args=['5'])
with col3:
    st.button("6", key="6" ,on_click=click_button, args=['6'])
with col4:
    st.button("\-", key="subtract" ,on_click=click_button, args=['-'])

# แถวที่ 4
with col1:
    st.button("1", key="1" ,on_click=click_button, args=['1'])
with col2:
    st.button("2", key="2" ,on_click=click_button, args=['2'])
with col3:
    st.button("3", key="3" ,on_click=click_button, args=['3'])
with col4:
    st.button("\+", key="add" ,on_click=click_button, args=['+'])

# แถวที่ 5
with col1:
    st.button("%", key="percent" ,on_click=click_button, args=['percent'])
with col2:
    st.button("0", key="0" ,on_click=click_button, args=['0'])
with col3:
    st.button(".", key="dot",on_click=click_button, args=['.'])
with col4:
    st.button("=", key="equal" ,on_click=click_button, args=['='])
