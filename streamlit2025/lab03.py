import streamlit as st
import plotly.express as px
import pandas as pd



''' #### example DataFrame ''' 
# ข้อมูลตัวอย่าง
data = {
    "เดือน": ["มกราคม", "กุมภาพันธ์", "มีนาคม","เมษายน"],
    "ยอดขาย": [200, 300, 250, 280],
    "กำไร(%)":[12,15,13,14]
}
df = pd.DataFrame(data)

# แสดงข้อมูลเป็นตาราง
st.dataframe(df)

# แสดงข้อมูลเป็นกราฟ
fig = px.bar(df, x="เดือน", y="ยอดขาย", title="ยอดขายรายเดือน")
st.plotly_chart(fig)




''' #### example ฟอร์ม ''' 
# ฟังก์ชันสำหรับรีเซ็ตฟอร์ม
def reset_form():
    st.session_state["input_text_name"] = ""
    st.session_state["input_num_age"] = 0 

if st.button("คลิกที่นี่"):
    st.success("คุณคลิกปุ่มแล้ว!")

# ตรวจสอบว่ามีการตั้งค่า session_state หรือยัง ถ้ายังไม่มีให้สร้าง
if "input_text_name" not in st.session_state:
    st.session_state["input_text_name"] = ""
    st.session_state["input_num_age"] = 0 

# การสร้างฟอร์มแบบโต้ตอบ
with st.form("my_form"):
    name = st.text_input("ชื่อ",key="input_text_name")
    age = st.number_input("อายุ",key="input_num_age" ,min_value=0, max_value=120)
    col1, col2 = st.columns(2)

    with col1:
      submit = st.form_submit_button("ส่งข้อมูล")
    with col2:  
      reset = st.form_submit_button("รีเซ็ต", on_click=reset_form)

    if submit:
        st.write(f"สวัสดี {name}, คุณอายุ {age} ปี")