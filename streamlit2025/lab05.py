import streamlit as st
from other_pages import home, report, settings  # นำเข้าโมดูลย่อย

# สร้าง Sidebar สำหรับเมนู
st.sidebar.title("เมนู")
page = st.sidebar.radio("ไปยังหน้า", ["หน้าแรก", "รายงาน", "ตั้งค่า"])

# ตรวจสอบเมนูที่เลือกและแสดงหน้าที่เกี่ยวข้อง
if page == "หน้าแรก":
    home.app()
elif page == "รายงาน":
    report.app()
elif page == "ตั้งค่า":
    settings.app()