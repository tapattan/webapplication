import streamlit as st
import time

import streamlit as st

# ตรวจสอบและสร้าง session_state
if 'YOURSCORE' not in st.session_state:
   st.session_state['YOURSCORE'] = 30 
   st.session_state['GAME_ACTION'] = False
   st.session_state['result_message'] = ""  # เก็บข้อความแสดงผล
   st.session_state['answer'] = ""  # เก็บค่าที่ตอบไว้

def submit_answer():
    if st.session_state['answer']:  
        ans = st.session_state['answer']  # ดึงค่าที่ผู้ใช้กรอก
        
        if 'thai' in ans.lower() or 'ไทย' in ans:
            st.session_state['result_message'] = f"✅ คุณทายถูกต้อง! ได้คะแนน {st.session_state['YOURSCORE']}"
        else:
            st.session_state['result_message'] = "❌ คุณทายผิด"

        st.session_state['GAME_ACTION'] = True
        st.rerun()  # รีเฟรชหน้าใหม่เพื่ออัปเดต UI

st.title(f"นี่คือประเทศอะไร {st.session_state['YOURSCORE']}")

# Text input ที่ไม่ล้างค่าเมื่อ rerun
st.text_input("คำตอบ:", key="answer", disabled=st.session_state['GAME_ACTION'])

# ปุ่มกดยืนยันคำตอบ (แทนการใช้ on_change + st.rerun())
if st.button("ยืนยันคำตอบ", disabled=st.session_state['GAME_ACTION']):
    submit_answer()
    st.rerun() 



# แสดงข้อความผลลัพธ์หลังจาก rerun
if st.session_state['result_message']:
    if "✅" in st.session_state['result_message']:
        st.success(st.session_state['result_message'])
    else:
        st.error(st.session_state['result_message'])


# สร้างปุ่มที่มีหลายปุ่มพร้อมเช็คสถานะ
buttons = ['แผ่นป้าย 1', 'แผ่นป้าย 2', 'แผ่นป้าย 3','แผ่นป้าย 4','แผ่นป้าย 5']
s = ['อยู่ทวีปเอเซีย','มีทะเลสวย','ไม่มีหิมะ','มีต้มยำกุ้ง','มีกีฬาที่มีชื่อประเทศข้างใน']
# ตรวจสอบว่าเคยคลิกปุ่มไหนบ้าง
for button in buttons:
    button_key = f'{button}_clicked'  # กำหนดชื่อ session_state ที่ไม่ซ้ำ
    if button_key not in st.session_state:
        st.session_state[button_key] = False  # ตั้งค่าเริ่มต้นเป็น False

    # แสดงปุ่มและเช็คการคลิก
    if not st.session_state[button_key]:
        if st.button(button,disabled=st.session_state['GAME_ACTION']):
            st.session_state[button_key] = True
            st.session_state['YOURSCORE'] -= 5 
            st.success(f"คุณได้คลิก {button}")
            st.rerun()  # รีเฟรชหน้าใหม่ทันทีหลังจากคลิก
    else:
        #if(button==buttons[0]):
         #   st.write(f"{button} ถูกซ่อนไว้แล้ว")
        for i in range (len(buttons)):
            if(buttons[i]==button):
                st.write(s[i])