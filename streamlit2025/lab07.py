import streamlit as st
import pandas as pd
import numpy as np

# ตั้งค่าหน้า
st.set_page_config(page_title="Market summary>", layout="wide")

# ข้อมูลตัวอย่างสำหรับดัชนี
indices = {
    "S&P 500": {"value": "5,836.21", "delta": "+0.16%", "change_color": "green"},
    "Nasdaq 100": {"value": "20,784.72", "delta": "-0.30%", "change_color": "red"},
    "Dow 30": {"value": "42,297.13", "delta": "+0.86%", "change_color": "green"},
    "Japan 225": {"value": "38,474.23", "delta": "-1.83%", "change_color": "red"},
    "FTSE 100": {"value": "8,234.53", "delta": "+0.13%", "change_color": "green"},
    "DAX": {"value": "20,298.27", "delta": "+0.82%", "change_color": "green"},
}

# ส่วนหัว
st.title("Market summary>")

# สร้างปุ่มที่มีสไตล์
selected_index = "S&P 500"
col1, col2, col3, col4, col5, col6 = st.columns(6)

for col, (label, data) in zip(
    [col1, col2, col3, col4, col5, col6], indices.items()):
     
    if col.button(label):
        selected_index = label
     

# แสดงกราฟเมื่อมีการเลือกดัชนี
st.subheader(f"Market Chart {selected_index}")
if selected_index:
    # ตัวอย่างข้อมูลจำลอง
    chart_data = pd.DataFrame(
        np.random.randn(100).cumsum(), columns=[selected_index]
    )
    st.line_chart(chart_data)
else:
    st.write("Please select an index to view the chart.")




# Community Ideas Section
st.subheader("Community Ideas")
tab1, tab2, tab3 = st.tabs(["Editors' picks", "For you", "Popular"])

with tab1:
    # แถวที่ 1: เนื้อหาสำหรับ Editors' Picks
    col1, col2, col3 = st.columns(3)
    for i in range(3):
        with col1:
            st.image("scn01.png", caption="Are CL Futures starting a new bull trend in 2025?")
            st.write("Crude Oil WTI Futures Update")
            st.caption("19 hours ago | 25 likes")

        with col2:
            st.image("scn02.png", caption="Lucid Stock Dips Under $3")
            st.write("Lucid Motors analysis update")
            st.caption("Jan 9 | 455 likes")

        with col3:
            st.image("scn03.png", caption="GBP/CAD - Triangle Breakout")
            st.write("Trading pattern analysis")
            st.caption("Jan 8 | 271 likes")

with tab2:
    st.write("Personalized content for you.")
with tab3:
    st.write("Trending topics in the market.")

 
st.write("Built with ❤️ by Streamlit")


