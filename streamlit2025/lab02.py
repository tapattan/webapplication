import streamlit as st
import plotly.express as px
import pandas as pd

''' #### example Layout Columns ''' 
# การจัด Layout ด้วย Columns
col1, col2 = st.columns([1, 2])  # กำหนดสัดส่วนของ Column
with col1:
    st.image("logo.png", width=100)
with col2:
    st.title("Dashboard การขาย")
    st.caption("รายงานยอดขายประจำเดือน")

''' #### example expander ''' 
# การใช้ Expander เพื่อซ่อนข้อมูลที่ไม่จำเป็น
lines = [
    "บรรทัดที่ 1",
    "บรรทัดที่ 2",
    "บรรทัดที่ 3"
]
with st.expander("รายละเอียดเพิ่มเติม"):
    for line in lines:
        st.write(line)


''' #### example markdown ''' 
# ใช้ HTML และ CSS ในการปรับแต่ง
st.markdown("""
    <style>
    .big-font {
        font-size:20px !important;
        color: #FF5733;
    }
    </style>
    <div class="big-font">นี่คือตัวอย่างข้อความขนาดใหญ่!</div>
""", unsafe_allow_html=True)


''' #### example radio '''
page = st.radio("เลือกหน้า", ["หน้าแรก", "รายงาน", "ตั้งค่า"])

if page == "หน้าแรก":
    st.title("ยินดีต้อนรับสู่แอปของเรา")
    st.write("ขอต้อนรับสู่บริษัทของเรา...")
elif page == "รายงาน":
    st.title("รายงาน")
    st.write("ข้อมูลรายงาน...")
elif page == "ตั้งค่า":
    st.title("ตั้งค่า")
    st.write("ปรับแต่งการตั้งค่าของคุณที่นี่...")

