import streamlit as st
import pandas as pd
import plotly.express as px

if "click_count" not in st.session_state:
    st.session_state["click_count"] = 0

def increment_counter():
    st.session_state["click_count"] += 1

# ปุ่มอัพเดตโดยอิงจากฟังก์ชัน
st.button("เพิ่มค่า", on_click=increment_counter)
st.write(f"คุณกดปุ่ม {st.session_state['click_count']} ครั้ง")