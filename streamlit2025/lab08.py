import streamlit as st

# สร้างลิสต์ของ URL หรือพาธไฟล์ของภาพ
image_list = [f"assets/{str(i+1).zfill(2)}.png" for i in range(10)]

st.title("Image Album Viewer 🐶")

# ตรวจสอบสถานะภาพปัจจุบัน
if "current_index" not in st.session_state:
    st.session_state.current_index = 0
    

# สร้างปุ่ม Next และ Previous
col1, col2, col3 = st.columns([1, 2, 1])
if col1.button("Previous"):
    if st.session_state.current_index > 0:
        st.session_state.current_index -= 1
 
if col3.button("Next"):
    if st.session_state.current_index < len(image_list)-1:
        st.session_state.current_index += 1
 
st.image(image_list[st.session_state.current_index], use_column_width=True)

# แสดงข้อมูลเพิ่มเติม
st.write(f"Image {st.session_state.current_index + 1} of {len(image_list)}")
st.write("Built with ❤️ by Streamlit")