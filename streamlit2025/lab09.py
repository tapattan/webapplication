import streamlit as st
import pandas as pd

st.set_page_config(layout="wide")

from other_pages import home, report, settings  # นำเข้าโมดูลย่อย

# สร้าง Sidebar สำหรับเมนู
st.sidebar.title("เมนู")
page = st.sidebar.radio("ไปยังหน้า", ["หน้าแรก", "รายงาน", "ตั้งค่า"])

# ตรวจสอบเมนูที่เลือกและแสดงหน้าที่เกี่ยวข้อง
if page == "หน้าแรก":
    home.app()
elif page == "รายงาน":
    report.app()
elif page == "ตั้งค่า":
    settings.app()
    

# สร้าง DataFrame
df = pd.read_csv('StockScreener_2025-01-18.csv')
df = df[~(df['Symbol'].str.contains('.R'))]
df = df[~(df['Symbol'].str.contains('.F'))]
df = df.set_index('Symbol')

# Session state สำหรับ Pagination และ Sorting
if "page_number" not in st.session_state:
    st.session_state.page_number = 0

if "sort_column" not in st.session_state:
    st.session_state.sort_column = df.columns[0]

if "ascending" not in st.session_state:
    st.session_state.ascending = True

# UI สำหรับเลือกอาชีพ
selected_occupation = st.multiselect("Filter by Industry", options=df["Industry"].unique(), default=df["Industry"].unique())

# กรองข้อมูลตามอาชีพที่เลือก
filtered_df = df[df["Industry"].isin(selected_occupation)]

# UI สำหรับ Sorting
sort_column = st.selectbox("Sort by column", filtered_df.columns, index=filtered_df.columns.get_loc(st.session_state.sort_column))
ascending = st.checkbox("Sort ascending", value=st.session_state.ascending)

# บันทึกค่า Sorting ลงใน Session State
st.session_state.sort_column = sort_column
st.session_state.ascending = ascending

# Sort ข้อมูลที่ถูกกรอง
sorted_df = filtered_df.sort_values(by=sort_column, ascending=ascending)

# กำหนดจำนวนแถวต่อหน้า
rows_per_page = 100

# คำนวณจำนวนหน้าทั้งหมด
total_pages = (len(sorted_df) - 1) // rows_per_page + 1

# Pagination Controls
col1, col2, col3 = st.columns([1, 2, 1])

with col1:
    if st.button("Previous") and st.session_state.page_number > 0:
        st.session_state.page_number -= 1

with col3:
    if st.button("Next") and st.session_state.page_number < total_pages - 1:
        st.session_state.page_number += 1

# ตัดข้อมูลสำหรับหน้าปัจจุบัน
start_idx = st.session_state.page_number * rows_per_page
end_idx = start_idx + rows_per_page
current_page_data = sorted_df.iloc[start_idx:end_idx]

# แสดงข้อมูลและสถานะหน้า
st.write(f"Page {st.session_state.page_number + 1} of {total_pages}")
st.dataframe(current_page_data,height=1000)