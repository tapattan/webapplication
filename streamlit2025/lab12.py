import streamlit as st

# ตั้งค่าหน้าต่างของ Streamlit
st.set_page_config(page_title="Secure Login App", page_icon="🔐", layout="centered")

# ฟังก์ชันตรวจสอบการล็อกอิน
def check_login():
    if "authenticated" not in st.session_state:
        st.session_state.authenticated = False

# ฟังก์ชันแสดงหน้า Login
def login_page():
    with st.form("login_form"):
        st.title("🔐 Login Page")
        username = st.text_input("Username", key="username_input")
        password = st.text_input("Password", type="password", key="password_input")
        submitted = st.form_submit_button("Login")
        if submitted:
            validate_login(username, password)

# ฟังก์ชันตรวจสอบข้อมูล login
def validate_login(username, password):
    VALID_CREDENTIALS = {
        "admin": "1234",
        "user1": "abcd",
    }
    if username in VALID_CREDENTIALS and VALID_CREDENTIALS[username] == password:
        st.session_state.authenticated = True
        st.session_state.logged_in_user = username
        st.experimental_rerun()
    else:
        st.error("Invalid Username or Password. Please try again!")

# ฟังก์ชันแสดงหน้า Dashboard
def dashboard():
    st.title("📊 Dashboard")
    st.write(f"Welcome **{st.session_state.logged_in_user}** 🎉")
    if st.button("🔓 Logout"):
        st.session_state.authenticated = False
        st.experimental_rerun()

# ตรวจสอบสถานะการล็อกอิน
check_login()

# แสดงหน้า Login หรือ Dashboard ตามสถานะการล็อกอิน
if st.session_state.authenticated:
    dashboard()
else:
    login_page()
