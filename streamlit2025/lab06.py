import streamlit as st
import datetime

# Header ของแอปพลิเคชัน
st.title("ตัวอย่าง Input Widgets ใน Streamlit")
st.write("รวมทุกประเภทของ Input Widgets พร้อมตัวอย่างการใช้งาน")

# ----------------------------------------------
# Button
st.subheader("1. Button")
if st.button("คลิกที่นี่"):
    st.write("คุณกดปุ่มแล้ว!")
# ----------------------------------------------

# Checkbox
st.subheader("2. Checkbox")
agree = st.checkbox("ฉันยอมรับเงื่อนไข")
if agree:
    st.write("ขอบคุณที่ยอมรับเงื่อนไข!")
# ----------------------------------------------

# Radio Buttons
st.subheader("3. Radio Buttons")
choice = st.radio("เลือกรายการ", ["ตัวเลือก 1", "ตัวเลือก 2", "ตัวเลือก 3"])
st.write(f"คุณเลือก: {choice}")
# ----------------------------------------------

# Selectbox
st.subheader("4. Selectbox")
option = st.selectbox("เลือกรายการ", ["ตัวเลือก A", "ตัวเลือก B", "ตัวเลือก C"])
st.write(f"คุณเลือก: {option}")
# ----------------------------------------------

# Multiselect
st.subheader("5. Multiselect")
options = st.multiselect("เลือกรายการ", ["ตัวเลือก 1", "ตัวเลือก 2", "ตัวเลือก 3"])
st.write(f"คุณเลือก: {options}")
# ----------------------------------------------

# Slider
st.subheader("6. Slider")
value = st.slider("เลือกค่า", 0, 100, 50)
st.write(f"ค่าที่เลือก: {value}")
# ----------------------------------------------

# Select Slider
st.subheader("7. Select Slider")
option = st.select_slider("เลือกวัน", options=["จันทร์", "อังคาร", "พุธ", "พฤหัส", "ศุกร์"])
st.write(f"คุณเลือก: {option}")
# ----------------------------------------------

# Text Input
st.subheader("8. Text Input")
text = st.text_input("กรอกข้อความ")
st.write(f"คุณป้อน: {text}")
# ----------------------------------------------

# Text Area
st.subheader("9. Text Area")
text_area = st.text_area("กรอกข้อความยาว")
st.write(f"ข้อความที่คุณป้อน:\n{text_area}")
# ----------------------------------------------

# Number Input
st.subheader("10. Number Input")
number = st.number_input("กรอกตัวเลข", min_value=0, max_value=100)
st.write(f"ค่าที่คุณป้อน: {number}")
# ----------------------------------------------

# Date Input
st.subheader("11. Date Input")
date = st.date_input("เลือกวันที่", datetime.date.today())
st.write(f"วันที่คุณเลือก: {date}")
# ----------------------------------------------

# Time Input
st.subheader("12. Time Input")
time = st.time_input("เลือกเวลา", datetime.time(12, 0))
st.write(f"เวลาที่คุณเลือก: {time}")
# ----------------------------------------------

# File Uploader
st.subheader("13. File Uploader")
uploaded_file = st.file_uploader("อัปโหลดไฟล์", type=["txt", "csv"])
if uploaded_file:
    st.write(f"ไฟล์ที่อัปโหลด: {uploaded_file.name}")
# ----------------------------------------------

# Color Picker
st.subheader("14. Color Picker")
color = st.color_picker("เลือกสี", "#00f900")
st.write(f"สีที่คุณเลือก: {color}")
# ----------------------------------------------

# Form
st.subheader("15. Form")
with st.form("my_form"):
    text = st.text_input("กรอกข้อความในฟอร์ม")
    number = st.number_input("กรอกตัวเลขในฟอร์ม", min_value=0, max_value=100)
    submit = st.form_submit_button("ส่งข้อมูล")
    if submit:
        st.write(f"ข้อความในฟอร์ม: {text}, ตัวเลขในฟอร์ม: {number}")
# ----------------------------------------------

# Footer
st.write("---")
st.write("นี่คือการรวมทุกประเภทของ Input Widgets ใน Streamlit")
