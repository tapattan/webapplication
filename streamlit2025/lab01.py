import streamlit as st
import yfinance as yf
import plotly.graph_objs as go
from datetime import datetime, timedelta

# ตั้งค่ารูปแบบหัวข้อ (Title)
st.title('ราคาหุ้นย้อนหลัง', anchor='left')

# สร้างส่วนค้นหาหุ้น
ticker = st.text_input('Enter a stock ticker symbol:')

# กำหนดช่วงเวลาย้อนหลัง 1 ปี
end_date = datetime.now()
start_date = end_date - timedelta(days=365)

# ดึงข้อมูลหุ้น
if ticker:
    data = yf.download(ticker, start=start_date, end=end_date)
    if not data.empty:
        st.write(f"Data for {ticker}")
        
        # สร้างกราฟโดยใช้ Plotly
        fig = go.Figure()
        fig.add_trace(go.Scatter(x=data.index, y=data['Close'], mode='lines', name='Close'))
        
        # ปรับแกน Y ให้เริ่มต้นจากค่าขั้นต่ำของข้อมูล
        min_y = data['Close'].min()
        max_y = data['Close'].max()
        fig.update_yaxes(range=[min_y * 0.95, max_y * 1.05])  # ขยายขอบเขตเล็กน้อยเพื่อความสวยงาม
        
        # แสดงกราฟใน Streamlit
        st.plotly_chart(fig)
    else:
        st.error("No data found for the given ticker.")