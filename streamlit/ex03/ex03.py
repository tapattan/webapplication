import streamlit as st
import pandas as pd
import numpy as np 
import plotly.express as px

import matplotlib.pyplot as plt


import tvDatafeed as tv

def load_data(symbol):
   obj = tv.TvDatafeed()
   k = obj.get_hist(symbol=symbol,exchange='SET',interval=tv.Interval.in_daily,n_bars=120)
   k = k[['close']]
   k = k.reset_index()
   return k 


st.markdown("# My first app Hello *world!*")


 

symbol = st.selectbox('view your stock : ',['none','KCE','TIDLOR','DUSIT','KLINIQ'])
if (st.checkbox('Are u Ok?',False)):
    
    # selectbox 
    if(symbol!='none'):
       #print(symbol)
       k = load_data(symbol)
       fig = px.line(k, x="datetime", y="close",line_shape="spline", render_mode="svg")
       st.write(fig)

 