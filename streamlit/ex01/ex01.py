import streamlit as st
import pandas as pd
import numpy as np 

import matplotlib.pyplot as plt

st.write("""
# My first app
Hello *world!*
""")
 
df = pd.read_csv('my_data.csv',index_col='DayNo')
df = df.reset_index() # ต้อง reset index ไม่งั้นไม่ออก
#df = df[['Cash']]

#st.line_chart(df)
st.write(df)
st.line_chart(df,y='Cash',x='DayNo')


fig, ax = plt.subplots()
ax.plot(df['DayNo'],df['Cash'])
plt.xticks(rotation = 45)
plt.show()
st.pyplot(fig)


chart_data = pd.DataFrame(
    np.random.randn(20, 3) + 200 ,
    columns=['a', 'b', 'c'])

st.line_chart(chart_data)